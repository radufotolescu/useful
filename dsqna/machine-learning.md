# Classification

## 1. How does classification work, and what is the approach to it?

1. Establish a cutoff probability for the class of interest above which we consider a record as belonging to that class.
2. Estimate the probability that a record belongs to the class of interest.
3. If that probability is above the cutoff probability, assign the new record to the class of interest.

## 2. Bayesian Classification

For each record to be classified:

1. Find all the other records with the same predictor value.
2. Determine what classes those records belong to and which class is more prevalent.
3. Assign that class to the new record.

## 3. Why Bayesian Classification is impractical?

When the number of features exceeds a handful, many of the records to be classified will be without matches.

## 4. Naive Bayes

In the Naive Bayes approach, we no longer restrict the probability calculation to those records that match the record to be classified. Instead we use the entire dataset.

1. For a binary outcome, we estimate the individual conditional probabilities for each predictor. These are the probabilities that the predictor value is in the record.
2. Multiply these probabilities by each other and then by the proportion of records belonging to the class of the outcome.
3. Repeat for all the classes.
4. Estimate a probability for an outcome by taking the calculated value for each class and dividing it by the sum of such values for all the classes.
5. Assign the record to the class with the highest probability for this set of predictor values.

## 5. Why is Naive Bayes naive?

Because we are assuming that all features are independent from one another.

## 6. What is covariance?

A measure of the extent to which a variable varies in line with another (in magnitude and direction).

Measures the relationship between two variables.

The covariance is given by sum of diferences between each variable and it's mean, divided by n - 1 number of records.

## 7. What is Logistic Regression?

Maps the relationship between multiple coefficients and their variables in order to reach a binary outcome.

It uses a function that maps the probability of belonging to a class with a range from -∞ to +∞ instead of 0 to 1.

## 8. What is Accuracy?

The percent of cases classified correctly.

(TP + TN) / (TP + TN + FP + FN)

## 9. What is Recall (Sensitivity)?

The percent of 1s correctly identified.

TP / (TP + FN)

## 10. What is Specificity?

The percent of 0s correctly identified.

TN / (TN + FP)

## 11. What is Precision?

The percent of predicted 1s that are actually 1s.

TP / (TP + FP)

## 12. What is the ROC curve?

A plot of Sensitivity over Specificity.

## 13. What is AUC (area under the ROC curve)?

A metric for the ability of a model to distinguish 1s from 0s.

It is simply the area under the ROC curve. The larger the value, the more effective the classifier is.

A completely ineffective classifier will have an AUC of 0.5, while a perfect classifier will have an AUC of 1.

## 14. How to deal with imbalanced data?

1. Balance the training data by undersampling the abundant case, or oversampling the rare case.
2. Bootstrap the rare cases or create synthetic data similar to existing rare cases.

# Regression

## 15. Simple Linear Regression

Simple linear regression models the relationship between the magnitude of one variable and that of a second.

y = mx + b

b = intercept
m = slope, coefficient

## 16. What are least squares?

The way linear regression fits the data. The regression line is the estimate that minimises the sum of squared residual values. (y - b - mx)

## 17. Multiple Linear Regression.

The linear model that maps the relationship between not just one but many features and their corresponding coefficients.

## 18. Define the root mean squared error (RMSE).

The most widely used metric to compare regression models.

## 19. Define R-squared.

The proportion of variance explained by the model (0 to 1).

## 20. What is Stepwise Regression?

Is a way to automatically determine which variables should be included in the model.

## 21. What is multicollinearity?

An extreme case of correlated variables, a condition in which there is redundance among the predictor variables.

## 22. Is multicollinearity an issue in tree based models?

No, although nonredundancy in features is good.

## 23. What is heteroskedaticity?

It is the lack of constant residual variance across the range of the predicted values. Errors are greater for some portions of the range than for others.

It indicates that prediction errors differ from different ranges of the predicted value, therefore suggesting an incomplete model.

## 24. What are partial residual plots?

Partial residual plots are a way to visualise how well the estimated fit explains the relationship between a predictor and the outcome.

# Machine Learning

## 25. What are the differences between Statistics and Machine Learning in the context of predictive modeling?

Machine learning tends to be more focused on developing efficient algorithms that scale to large data in order to optimize the predictive model.

Statistics focuses more on the probabilistic theory and underlying structure of the model.

## 26. What is the Euclidean distance?

Is a distance metric that measures how far two vectors are from one another, in a straight line.

To measure it substract the two vectors from one another, square the differences, sum them and take the square root.

## 27. What is the Manhattan distance?

Is the distance between two points traversed in a single direction at a time.

## 28. Define Standardization (z-score).

Standardization puts all variables on the same scale by subtracting the mean and dividing by the standard deviation.

## 29. Is it safe to use one hot encoding with linear and logistic regression?

No as it causes problems with multicollinearity. In these cases one dummy needs to be ommited.

## 30. Bias-Variance tradeoff

Variance refers to the modeling error that occurs because of the choice of training data.

Bias refers to the modeling error that occurs because of the sampling process.

When a model is overfit, the variance increases, but when you reduce the complexity of the model you increase bias.

## 31. What is Machine Learning?

Machine Learning is the science and art of programming computers so that they learn from data.

A program is said to learn from experience E with respect to some task T and some performance measure P, if its performance on T, as measured by P, improves with experience E.

## 32. What is Machine Learning good for?

1. Problems for which existing solutions require a lot of fine tuning or long lists of rules.
2. Complex problems for which using a traditional approach yields no good solution.
3. An ML algorithm can adapt to new data, can deal with fluctuating environments.
4. Getting insights about complex problems and large amounts of data.

## 33. What is Supervised Learning?

Is a category of machine learning algorithms where the training set you feed to the algorithm includes the desired solutions, the labels.

## 34. What is Unsupervised Learning?

Is a category of machine learning algorithms where the training data is unlabeled.

## 35. What is Semisupervised Learning?

With semisupervised learning, you have both labeled and unlabeled instances.

## 36. What is an online learning system?

This type of system can learn incrementally, as opposed to batch learning.

This makes it capable of adapting rapidly to both changing data and of training on very large quantities of data.

## 37. What is out of core learning?

Out of core algorithms can handle vast quantities of data that cannot fit into memory.

An out of core learning algorithm chops the data into mini batches as uses online learning techniques to learn from those mini batches.

## 38. What's the difference between a model parameter and a learning algorithm's hyperparameter?

A model has one or multiple parameters that determine what it will predict given a new instance. A learning algorithm tries to find optimal values for these parameters.

A hyperparameter of the other hand is a parameter of the learning algorithm itself not of the model (ex. regularization, dropout).

## 39. Why would you use a test set?

A test set is used to estimate the generalization error that a model will make on new instances.

## 40. What's the purpose of a validation test?

A validation set is used to compare models, it makes it possible to select the best model and tune hyperparameters.

## 41. How to deal with missing values?

Most machine learning algorithms cannot work with missing values. There are three options:

1. Get rid of the whole feature
2. Get rid of the record that contains a null value
3. Set the value to some value (0, mean, median, etc)

## 42. Why use One Hot Encoding?

If we just convert categories text to numbers, an issue we can encounter is that the machine learning algorithm will assume that two nearby values are more similar, that they are ordered by importance.

By one hot encoding, we create one binary attribute per category, one attribute will be 1 while the others 0 and so on for each category.

## 43. Why is feature scaling important?

With few exceptions, machine learning algorithms don't perform well when the input numerical attributes have very different scales.

There are two common ways of doing this:

1. Min-max scaling (normalization) - subtracting the min value and dividing by the max minus the min (returns values between 0 and 1)
2. Standardization - subtracting the mean value and then dividing by the standard deviation.

## 44. Which feature scaling method deals better with outliers?

Standardization.

## 45. When is accuracy not a good classification metric?

When we are dealing with imbalanced data, where we have some classes that are more frequent than others.

## 46. What is a confusion matrix?

Each row in a confusion matrix is represented by an actual class, with each column representing a predicted class.

TN | FP
/ FN | TP

## 47. Why use F1 score instead of accuracy?

F1 Score is the weighted average of Precision and Recall. Therefore, this score takes both false positives and false negatives into account.

Intuitively it is not as easy to understand as accuracy, but F1 is usually more useful than accuracy, especially if you have an uneven class distribution.

F1 Score = 2*(Recall * Precision) / (Recall + Precision)

F1 Score keeps a balance between Precision and Recall. We use it if there is uneven class distribution, as precision and recall may give misleading results.

## 48. Why use Precision and Recall instead of the F1 score?

F1 Score favors classifiers that have a similar precision and recall.

If the cost of false positives and false negatives are very different, it’s better to look at both Precision and Recall.

## 49. What is Gradient Descent?

Gradient descent is a generic optimization algorithm that tweaks parameters iteratively in order to minimize a cost function.

An important parameter in GD is the learning rate that defines the size of the step.

If the learning rate is too small, the algorithm will take a long time to converge.
If the learning rate is too high, the algorithm might diverge, failing to find an optimal solution.
